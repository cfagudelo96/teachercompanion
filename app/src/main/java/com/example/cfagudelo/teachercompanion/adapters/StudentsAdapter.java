package com.example.cfagudelo.teachercompanion.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.cfagudelo.teachercompanion.R;
import com.example.cfagudelo.teachercompanion.models.Student;

import java.util.ArrayList;

public class StudentsAdapter extends RecyclerView.Adapter<StudentsAdapter.ViewHolder> {
    private ArrayList<Student> students;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mStudentNameTextView;

        public ViewHolder(View view) {
            super(view);

            mStudentNameTextView = view.findViewById(R.id.list_item_student_name);
        }

        public TextView getStudentNameTextView() {
            return mStudentNameTextView;
        }
    }

    public StudentsAdapter(ArrayList<Student> students) {
        this.students = students;
    }

    @Override
    public StudentsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_student, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Student student = students.get(position);
        holder.getStudentNameTextView().setText(student.getName());
    }

    @Override
    public int getItemCount() {
        return students.size();
    }
}
