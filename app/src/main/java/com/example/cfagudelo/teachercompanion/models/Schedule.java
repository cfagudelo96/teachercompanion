package com.example.cfagudelo.teachercompanion.models;

import java.util.Date;

public class Schedule {
    private Date startDate;
    private Date finishDate;
    private String place;
    private Subject subject;
}
