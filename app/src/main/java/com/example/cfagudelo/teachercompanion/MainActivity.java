package com.example.cfagudelo.teachercompanion;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.cfagudelo.teachercompanion.adapters.ClassroomsAdapter;
import com.example.cfagudelo.teachercompanion.models.Classroom;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mRecyclerView = findViewById(R.id.recycler_view_classrooms);

        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        ArrayList<Classroom> classrooms = new ArrayList<>();
        for (int i = 1; i <= 3; i++) {
            int imageDrawableId = this.getResources().getIdentifier("classroom_" + i, "drawable", this.getPackageName());
            Classroom classroom = new Classroom("3ero - " + i, imageDrawableId);
            classrooms.add(classroom);
        }

        mAdapter = new ClassroomsAdapter(classrooms);
        mRecyclerView.setAdapter(mAdapter);
    }
}
