package com.example.cfagudelo.teachercompanion.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.cfagudelo.teachercompanion.R;
import com.example.cfagudelo.teachercompanion.models.Classroom;

import java.util.ArrayList;

public class ClassroomsAdapter extends RecyclerView.Adapter<ClassroomsAdapter.ViewHolder> {
    private ArrayList<Classroom> classrooms;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mClassroomNameTextView;
        private ImageView mClassroomImageView;

        public ViewHolder(View view) {
            super(view);

            mClassroomNameTextView = view.findViewById(R.id.list_item_classroom_name);
            mClassroomImageView = view.findViewById(R.id.list_item_classroom_image);
        }

        public TextView getClassroomNameTextView() {
            return mClassroomNameTextView;
        }

        public ImageView getClassroomImageView() {
            return mClassroomImageView;
        }
    }

    public ClassroomsAdapter(ArrayList<Classroom> classrooms) {
        this.classrooms = classrooms;
    }

    @Override
    public ClassroomsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_classroom, parent, false);
        ClassroomsAdapter.ViewHolder viewHolder = new ClassroomsAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ClassroomsAdapter.ViewHolder holder, int position) {
        Classroom classroom = classrooms.get(position);
        holder.getClassroomNameTextView().setText(classroom.getName());
        holder.getClassroomImageView().setImageResource(classroom.getImageDrawable());
    }

    @Override
    public int getItemCount() {
        return classrooms.size();
    }
}
