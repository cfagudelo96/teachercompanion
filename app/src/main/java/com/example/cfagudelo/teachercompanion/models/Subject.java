package com.example.cfagudelo.teachercompanion.models;

public enum Subject {
    MATH,
    SCIENCE,
    SOCIAL_STUDIES,
    SPANISH,
    ENGLISH,
    COMPUTING,
    ART,
    PHYSICAL_EDUCATION
}
